﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TPOApplication.Startup))]
namespace TPOApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
